var saveSettings = document.getElementById("input-save");

saveSettings.addEventListener("click",SaveSettings);

var timeW = document.getElementById("timeWork");
var timeB = document.getElementById("timeBreak");
var timeLB = document.getElementById("timeLongBreak");
var countTimes = document.getElementById("countTimes");

function SaveSettings(){
        $.ajax({
            type:'POST',
            url:"/projects/pomodoro.com/user/ajax/saveSettings",
            dataType:"text",
            data:{
              'timeWork':timeW.value,
              'timeBreak':timeB.value,
              'timeLongBreak':timeLB.value,
              'countTimes':countTimes.value
            },
            cache: false,
            success:function(msg){
             //  text = msg.split(/\r?\n+/);
             $('#result-saving').html(msg);
          }
        });
}

timeW.onkeypress = onlyFigures; 
timeB.onkeypress = onlyFigures;
timeLB.onkeypress = onlyFigures; 
countTimes.onkeypress = onlyFigures; 


function onlyFigures(e) {
  e = e || event;

  if (e.ctrlKey || e.altKey || e.metaKey) return;

  var chr = getChar(e);

  // с null надо осторожно в неравенствах,
  // т.к. например null >= '0' => true
  // на всякий случай лучше вынести проверку chr == null отдельно
  if (chr == null) return;

  if (chr < '0' || chr > '9') {
    return false;
  }
}

 function getChar(event) {
      if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
      }

      if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which) // остальные
      }

      return null; // специальная клавиша
    }