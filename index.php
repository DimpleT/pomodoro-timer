<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", dirname(__FILE__));
define("VIEWS_PATH", ROOT.DS.'views');
define("PROJECT_PATH", "/projects/pomodoro.com");

require_once(ROOT.DS."lib".DS."init.php");

//$router = new Router($_SERVER['REQUEST_URI']);
session_start();

App::run($_SERVER['REQUEST_URI']);


//require_once(ROOT."/components/Router.php");

//require_once("conf/config.php");

//$router = new Router();
//$router->run();