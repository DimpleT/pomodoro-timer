<?php

class MyAjax extends Model{

	public function saveTime(){
		if( Session::get('id') && Session::get('id') != '' && isset($_POST['timeWork'])){
  			$id = Session::get('id');
  			$timeWork = $_POST['timeWork'];

  			$hours = floor($timeWork / 60);
  			$minutes = $timeWork % 60;
  
 			$timeWork = $hours.":".$minutes.":00";

  			$date_today = date("Y-m-d");
  			$time = date("H:i:s"); 

  			$datetime = $date_today." ".$time;

    		$sql = "Insert into statistics (userId,date,timeWork) values('{$id}','{$datetime}','{$timeWork}');";
    		$result = $this->db->query($sql);
    		if(!$result){
     			//echo 'invalid ';
      			//echo $sql;
			}

		} else {
  			//echo "No ID";
		}
	}


  public function saveSettings(){
    if( Session::get('id') && Session::get('id') != ''){
      if(isset($_POST['timeWork']) && $_POST['timeWork'] != '' && 
        isset($_POST['timeBreak']) && $_POST['timeBreak'] != '' &&
        isset($_POST['timeLongBreak']) && $_POST['timeLongBreak'] != '' &&
        isset($_POST['countTimes']) && $_POST['countTimes'] != '' ){
        $id = Session::get('id');
        
        $timeWork = $_POST['timeWork'];
        $timeWork = floor($timeWork / 60).":".($timeWork % 60).":00";

        $timeBreak = $_POST['timeBreak'];
        $timeBreak= floor($timeBreak / 60).":".($timeBreak % 60).":00";
        
        $timeLongBreak = $_POST['timeLongBreak'];
        $timeLongBreak= floor($timeLongBreak / 60).":".($timeLongBreak % 60).":00";

        $countTimes = $_POST['countTimes'];

        $sql = "update settings set timeWork='{$timeWork}',timeBreak='{$timeBreak}',longBreak='{$timeLongBreak}',countTimes='{$countTimes}' WHERE userId={$id};";
        $result = $this->db->query($sql);
        if(!$result){
          //echo 'invalid ';
            //echo $sql;
        } else {
          echo "Saved";
        }

      } else {
        echo "Some fields are empty";
      }
    }
  }

}