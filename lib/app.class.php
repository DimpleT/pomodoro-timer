<?php

class App{

	protected static $router;

	public static $db;

	public static function getRouter(){
		return self::$router;
	}

	public static function run($uri){
		self::$router = new Router($uri);

		self::$db = new DB(Config::get('db.host'),Config::get('db.user'),Config::get('db.password'),Config::get('db.db_name'));

		$controller_class = ucfirst(self::$router->getController()).'Controller';
		$controller_method = strtolower(self::$router->getMethodPrefix().self::$router->getAction());

		$layout = self::$router->getRoute();

		if( $layout == "user" && Session::get('role') != 'user' ){
			if( !empty($_COOKIE['login']) and !empty($_COOKIE['id']) ) {
				Session::set('role', 'user'); 
				Session::set('id', $_COOKIE['id']); 
				Session::set('login', $_COOKIE['login']); 
			} elseif( $controller_method != 'login' ){
				Router::redirect(PROJECT_PATH.'/users/login');
			}
		} 

		if( $layout == "default" && Session::get('role') == 'user'){
			if($controller_method != 'login'){
				Router::redirect(PROJECT_PATH."/user/");
			}
		}

		// Check ajax
		if(self::$router->getController() == "ajax"){
			//Calling ajax controller's method
			$controller_object = new $controller_class();
			if(method_exists($controller_object, $controller_method)){
				$controller_object->$controller_method();
			}
		} else{
			//Calling controller's method
			$controller_object = new $controller_class();

			if(method_exists($controller_object, $controller_method)){
				//Controller's may return a view path
				$view_path = $controller_object->$controller_method();
				$view_object = new View($controller_object->getData(), $view_path);
				$content = $view_object->render();
			} else {
				throw new Exception("Method {$controller_method} of class {$controller_class} does not exist ");
			}

			$layout_path = VIEWS_PATH.DS.$layout.'.html';
			$layout_view_object = new View(compact('content'), $layout_path);
			echo $layout_view_object->render();
		}
	}
}