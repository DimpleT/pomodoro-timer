<?php

class TimerController extends Controller{

	public function __construct($data = array()){
		parent::__construct($data);
		$this->model = new Timertime();
	}

	public function index(){
		//$this->data['test_content'] = "This is a timer list";
	}

	public function user_index(){
		$this->data['countPomadoro'] = $this->model->getCountPomadoros();
		$this->data['settings'] = $this->model->getSettings();
	} 
}